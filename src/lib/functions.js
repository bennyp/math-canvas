{
const fromDom = (element) => ({
  symbol: element.innerHTML,
  type: element.getAttribute('type'),
  row: element.getAttribute('row'),
  col: element.getAttribute('col'),
  height: element.getAttribute('height'),
  width: element.getAttribute('width'),
  index: element.getAttribute('index'),
});

const sortBy = (prop) => (xs) => xs.sort((a, b) =>
  a[prop] < b[prop] ? -1
  : a[prop] > b[prop] ? 1
  : 0);

const groupBy = (prop) => (xs) =>
  xs.reduce((rv, x) =>
    Object.assign(rv, {[x[prop]]: [...(rv[x[prop]] || []), x]}), {});

const compose = (...fns) => fns.reduce((f, g) => (...args) => f(g(...args)));

const filter = (f) => (xs) => xs.filter(f);

const map = (f) => (x) => x.map(f);

const tileOnCanvas = (el) =>
  el.hasAttribute('on-canvas') &&
  el.localName === 'tile';

window.MathCanvasLib = window.MathCanvasLib || {};
MathCanvasLib.fromDom = fromDom;
MathCanvasLib.sortBy = sortBy;
MathCanvasLib.groupBy = groupBy;
MathCanvasLib.compose = compose;
MathCanvasLib.filter = filter;
MathCanvasLib.map = map;
MathCanvasLib.tileOnCanvas = tileOnCanvas;
}
