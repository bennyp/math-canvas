const cors = require('cors')({origin: true});
const functions = require('firebase-functions');
const admin = require('firebase-admin');
      admin.initializeApp(functions.config().firebase);

const stripe = require('stripe')(functions.config().stripe.token);

// Payments API

const payment = (req, res) => {
  const {source, currency, description} = req.body;
  const amount = req.body.amount * 100;
  const receipt_email = req.body.email; // eslint-disable-line camelcase
  const donation = {amount, currency, description, receipt_email, source};

  stripe.charges.create(donation, (error, charge) => {
    if (error) console.log(error);
    res.status(error ? 400 : 200).send({error, charge});
  });
};

const onRequest = (req, res) => cors(req, res, () => payment(req, res));

exports.payment = functions.https.onRequest(onRequest);
